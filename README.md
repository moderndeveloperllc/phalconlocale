PhalconLocale
=============

# Abandonware

**This code was written for Phalcon v1, so you may be able to find better PHP and Phalcon funtions that can do this. I'm leaving here for reference in case anyone wants to think about doing URL redirects based on locales, etc.**

Locale class to use with Phalcon 1. Provides static class URL redirection in addition to obtaining the client locale. It should be noted that no automated locale determination is 100%. If you have multiple locales for your site, always, ***always*** provide an option to manually set the locale.

-------------

Installing via Composer
=======================
Install composer in a common location or in your project:

```bash
curl -s http://getcomposer.org/installer | php
```

Create the composer.json file as follows:

```json
{
    "require": {
        "moderndeveloperllc/phalconlocale" : "1.0.*"
    }
}
```

Run the composer installer:

```bash
php composer.phar install
```

---------------

Config File (or bootstrap if there)
-------------
```
<?php
return new \Phalcon\Config(array(
    // Other, existing config options
    'locale' => array(
        'localeBasedRedirect' => true,
        'defaultLocale' => 'en_US',
        'sessionKey' => 'applocale',
        'doubleCheckBrowserDefaultLangs' => true,
        'useGeoIP' => true,
        'geoIPEdition' => GEOIP_COUNTRY_EDITION,
        'availableLocales' => array(
            'de',
            'en_US',
            'en',
            'es_419',
            'es_ES',
            'es',
            'fr_FR'
        )
    )
));
```

The PhalconLocale class, both the routing static function and the instantiated class, depend upon a locale array added to the main Phalcon config array.

**localeBasedRedirect** - Set to ``false`` to turn off ``urlRedirect()`` functionality. Useful for testing purposes.

**defaultLocale** - Default locale to use if all methods fail, or user's locale does not match any **availableLocales**.

**sessionKey** - Session key with which the locale is stored. Both the static function and instantiated class use the session to store the locale after it has been determined. *Use this key to also store any locale that the client has manually set through your site's interface.*

**doubleCheckBrowserDefaultLangs** - Older browsers many times came with the default Accept-Languages set to en-us and en. If this is set to ``true``, then we can check the client's IP address to determine a country of origin. Of limited utility if **useGeoIp** is set to ``false``.

**useGeoIp** - If you have the [GeoIP](http://php.net/geoip) extension installed, we can use this to help determine a valid locale for the client. This is used if the primary browser language is not in **availableLocales** or the language is en or en-us *and* **doubleCheckBrowserDefaultLangs** is set to ``true``.

**geoIPEdition** - A [GeoIP edition constant](http://www.php.net/manual/en/geoip.constants.php). Used to determine that you have properly installed GeoIP and we can reach it. Only needed if **useGeoIP** is set to ``true``.

**availableLocales** - A list of valid locales for your site. If the locale logic cannot find the client's locale in this list, the **defaultLocale** will be used. The **defaultLocale** should be included in this list too as that will save a few rounds of logic if it is the client's locale.

-------------

Router file (or bootstrap if you do it there)
-------------
```
<?php
use \Phalcon\Mvc\Router;
$router = new Router();

//Your existing routes

/*
 * Locale aware routes
 */
$withLocale = new \Phalcon\Mvc\Router\Group();

$withLocale->setPrefix('/{locale:[a-z]{2,3}([\_\-][[:alnum:]]{1,8})?}');

//Need to add in the default Phalcon routes to be locale aware
$withLocale->add(
    '/',
    array(
        'controller' => 'index',
        'action' => 'index',
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);

$withLocale->add(
    '/:controller',
    array(
        "controller" => 2,
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);

$withLocale->add(
    '/:controller/:action',
    array(
        "controller" => 2,
        "action"     => 3
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);

$withLocale->add(
    '/:controller/:action/:params',
    array(
        "controller" => 2,
        "action"     => 3,
        "params"     => 4,
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);

//Add locale-aware versions of your existing routes
$withLocale->add(
    '/confirm/{code}/{email}',
    array(
        'controller' => 'user_control',
        'action' => 'confirmEmail',
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);

$withLocale->add(
    '/reset-password/{code}/{email}',
    array(
        'controller' => 'user_control',
        'action' => 'resetPassword',
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);

$router->mount($withLocale);
return $router;
```

If you are doing url redirection with the static class, you will need to set up the above router group **below** the rest of your routes. You will also need to add any custom routes used in your application. The name for all the routes is the same: a constant from the class. This name is used by the logic to determine if we have already added a locale onto the front of the url. **Note**: The group prefix will add a route segment as the first element in the route. This is why a route like this:

```
<?php
$withLocale->add(
    '/:controller/:action/:params',
    array(
        "controller" => 2,
        "action"     => 3,
        "params"     => 4,
    )
)->setName(\ModDev\PhalconLocale\PhalconLocale::LOCALE_AWARE_ROUTE);
```

will start with a **2** in the named array. This is important, and your routes will not work if you start with a **1** in this array.

-------------

Services File (or bootstrap if there)
-------------
```
<?php
// Set a locale service to use in your app.
$di->set(
    'locale',
    function () use ($di, $config) {
        return new PhalconLocale($di, $config);
    },
    true
);
```

You can optionally add a service to retrieve the locale from any DI-aware class. This is useful when creating locale-aware links inside your application when doing locale-based redirection. It is also useful for knowing which translations to use, etc. You will need to instantiate the class with your DI object, and the Phalcon\Config object you used to store your locale configs.

-------------

Loader file
-------------

**Library Installed Manually**

```
<?php
//Make sure to register namespace
$loader->registerNamespaces(array(
    //Other namespaces
  'ModDev\PhalconLocale' => $path->to->library . '/src/ModDev/PhalconLocale'
));
```

If you install the library manually, be sure to load the library in your autoloader. The easiest to do is in a `registerNamespaces()` function. There is only one class at the moment, but that could change in the future.

**Library Installed via Composer**
```
<?php
// Your existing loader config...
// ...

require_once __DIR__ . '/../../vendor/autoload.php';
```

-------------

In your bootstrap file
-------------
```
<?php
//Configure all of your routes, services, configurations

/**
 * Locale based-redirection. Requires the dependency injection and configuration classes.
 */
ModDev\PhalconLocale\PhalconLocale::urlRedirect($di, $config);

//Actually run your MVC application

/**
 * Handle the request
 */
$application = new \Phalcon\Mvc\Application($di);

echo $application->handle()->getContent();
```

Locale-based URL redirection is not required when using this class. The class can be instantiated on it's own to get the client's locale for items such as translations, etc.